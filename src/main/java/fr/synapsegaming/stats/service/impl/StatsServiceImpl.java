package fr.synapsegaming.stats.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.synapsegaming.user.dao.ClazzDao;
import fr.synapsegaming.user.dao.RaceDao;
import fr.synapsegaming.user.dao.SpecializationDao;
import fr.synapsegaming.user.dao.UserDao;
import fr.synapsegaming.user.entity.Clazz;
import fr.synapsegaming.user.entity.Race;
import fr.synapsegaming.user.entity.User;
import fr.synapsegaming.user.entity.Specialization;
import fr.synapsegaming.stats.service.StatsService;

@Service("StatsService")
public class StatsServiceImpl implements StatsService{

	@Autowired
	ClazzDao clazzDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	RaceDao raceDao;
	
	@Autowired
	SpecializationDao specializationDao;


	@Override
	public int getNumberOfUsersByClazz(Clazz clazz){
		return clazz.getUsers().size();
	}
	
	@Override
	public int getNumberOfUsersByRace(Race race){
		return race.getUsers().size();
	}
	
	@Override
	public int getNumberOfUsersBySpecialization(Specialization specialization){
		return specialization.getUsers().size();
	}
	
	@Override
	public HashMap<Clazz, Integer> listXMostPlayedClasses(int nbClasses) {
		
		HashMap<Clazz, Integer> classUseList = new HashMap<>();
		HashMap<Clazz, Integer> classUseListSort = new HashMap<>();
		List<Clazz> classes = clazzDao.list(Clazz.class);
		if(nbClasses > classes.size() || nbClasses<1){
			nbClasses = classes.size();
		}
		for(int i=0; i<classes.size(); i++)
		{
			classUseList.put(classes.get(i), getNumberOfUsersByClazz(classes.get(i)));
		}
		
		for(int i=0; i<classUseList.size(); i++)
		{
			if(classUseListSort.get(i) == null)
			{
				
			}
			classUseListSort.put(classes.get(i), getNumberOfUsersByClazz(classes.get(i)));
		}
		
	
		return classUseList;
	}
	
	@Override
	public HashMap<Race, Integer> listXMostPlayedRaces(int nbRaces) {
		
		HashMap<Race, Integer> raceUseList = new HashMap<>();
		List<Race> races = raceDao.list(Race.class);
		
		for(int i=0; i<races.size(); i++)
		{
			raceUseList.put(races.get(i), getNumberOfUsersByRace(races.get(i)));
		}
	
		return raceUseList;
	}

	
	@Override
	public HashMap<Specialization, Integer> listXMostPlayedSpecialization(int nbSpecializations) {
		
		HashMap<Specialization, Integer> specializationUseList = new HashMap<>();
		List<Specialization> specialization = specializationDao.list(Specialization.class);
		
		for(int i=0; i<specialization.size(); i++)
		{
			specializationUseList.put(specialization.get(i), getNumberOfUsersBySpecialization(specialization.get(i)));
		}
	
		return specializationUseList;
	}

	@Override
	public List<User> listUsersWithoutAvatar(){
		List<User> users = userDao.list(User.class);
		
		List<User> usersWithoutAvatar = new ArrayList<>();
		
		for(int i=0; i<users.size(); i++)
		{
			if( users.get(i).getForumAvatar() == "/resources/img/default_avatar.png" )
			{
				usersWithoutAvatar.add(users.get(i));
			}
		}
		return usersWithoutAvatar;
	}
}


